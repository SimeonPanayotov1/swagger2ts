import { join } from "path";
import { FileService, LogService } from "../services";
import { IEndPoint, IEndPointsFile, IGeneratorArgs, IParsedEndPoint, IPath } from "./generator.models";

export default class EndPointsGenerator {
  [x: string]: any;
  private paths?: IPath;
  private fileService: FileService;
  private fileName: string;
  private logService: LogService;
  private destinationFolderName: string;
  private typesDict: { [name: string]: string } = {
    integer: "number",
    string: "string"
  };

  constructor(args: IGeneratorArgs) {
    this.paths = args.paths;
    this.fileService = args.fileService;
    this.logService = args.logService;
    this.fileName = args.fileName;
    this.destinationFolderName = args.destinationFolderName;
  }

  public generate = () => {
    this.fileService.writeToFile(
      this.fileName,
      this.getStringifiedEndpoints(),
      join(__dirname, "../", "templates", "export-object.ts"),
    )
      .then(() => {
        this.logService.log(`${this.destinationFolderName}/${this.fileName} was generated!`);
      });
  }

  private getStringifiedEndpoints(): string {
    return this.getParsedStringJson(this.extractEndPoints());
  }

  private getParsedStringJson(json: object): string {
    return JSON.stringify(json, null, 2)
      // strip getUrl string function from quotes
      .replace(new RegExp('"getUrl": "([^"]*)"', "g"), '"getUrl": \$1')
      // remove quotes from object keys
      .replace(/"(\w+)"\s*:/g, "$1:")
      // replace all double quotes with single
      .replace(/"/g, "'")
      .replace(/},/g, "},\n");
  }

  private extractEndPoints(): IEndPointsFile {
    if (!this.paths) {
      return {};
    }
    const endpoints: IEndPointsFile = {};
    Object.keys(this.paths).forEach((url: string) => {
      const path = this.paths && this.paths[url];
      if (!path) {
        return;
      }
      const parsedUrl = this.getParsedUrl(url);

      Object.keys(path).forEach((method: string) => {
        const endpoint: IEndPoint = path[method];
        if (endpoint.operationId) {
          endpoints[endpoint.operationId] = this.getParsedEndPoint(endpoint, parsedUrl, method);
        }
      });
    });
    return endpoints;
  }

  private getParsedUrl(url: string): string {
    let str = "";
    // tslint:disable-next-line prefer-for-of
    for (let index = 0; index < url.length; index++) {
      if (url[index] === "{") {
        str += "$";
      }
      str += url[index];
    }

    const paramStartIndex = str.indexOf("{");
    const paramEndIndex = str.indexOf("}");
    if (paramStartIndex > -1) {
      const originalParam = str.substring(paramStartIndex + 1, paramEndIndex);
      str = str.replace(originalParam, this.underscore(originalParam));
    }
    return str;
  }

  private underscore = (str: string) => str.replace(new RegExp("-", "g"), "_");

  private getParsedEndPoint(endpoint: IEndPoint, url: string, method: string): IParsedEndPoint {
    const parsedEndPoint: IParsedEndPoint = { method };

    if (endpoint.parameters) {
      const queryParams = endpoint.parameters
        .filter((p) => p.in === "path")
        .map((p) => {
          const type = p.type || p.schema.type || "";
          return `${this.underscore(p.name)}: ${this.typesDict[type] || type}`;
        })
        .join(", ");
      if (queryParams) {
        const stringFunction = "(" + queryParams + ") => `" + url + "`";
        parsedEndPoint.getUrl = stringFunction;
      }
    }

    if (!parsedEndPoint.getUrl) {
      parsedEndPoint.url = url;
    }

    if (endpoint.consumes) {
      parsedEndPoint.contentType = endpoint.consumes[0];
    }
    if (endpoint.requestBody) {
      const { content } = endpoint.requestBody;
      parsedEndPoint.contentType = Object.keys(content)[0];
    }

    return parsedEndPoint;
  }
}
