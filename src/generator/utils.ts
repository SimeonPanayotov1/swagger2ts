
export const getSchemaName = (schemaUrl: string) => {
  const parts = schemaUrl.split("/");
  return parts[parts.length - 1];
};

export const capitalizeFirstLetter = (str: string) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

export const capitalize = (str: string) => {
  if (str.split("_").length > 1) {
    return str.split("_").reduce((prevVal, currentVal) => {
      return capitalizeFirstLetter(prevVal) +
        capitalizeFirstLetter(currentVal);
    });
  }
  return capitalizeFirstLetter(str);
};
