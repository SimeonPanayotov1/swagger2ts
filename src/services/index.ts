import FileService from "./file.service";
import LogService from "./log.service";

export {
  FileService,
  LogService
};
