export interface {{ interface_name }} {
  {{ interface_definitions }}
}